#include <stdio.h>

int a [100][100],product[100][100],b[100][100],addition[100][100];
	int arows,acols,brows,bcols;
	int i,j,k;
	int sum=0;

int main(){

	printf("Enter the rows and columns of matrix a: \n");
	scanf("%d %d",&arows,&acols);

	printf("Enter the elements of matrix a:\n");
	for(i=0;i<arows;i++){
		for(j=0;j<acols;j++){
			scanf("%d",&a[i][j]);
		}
	}

	printf("Enter the rows and columns of matrix b: \n");
	scanf("%d %d",&brows,&bcols);

	//for multiplication of matrices matrix b rows must be equals to matrix a columns
	if(brows !=acols){
		printf("Sorry! cannot multiply the matrices a & b");
        }
	else{
	   printf("Enter the elements of matrix b: \n");
	   for(i=0;i<brows;i++){
	   	for(j=0;j<bcols;j++){
	   		scanf("%d",&b[i][j]);
		   }
	   }
	}
       printf("\n");

	//multiplication of matrices
	for(i=0;i<arows;i++){
		for(j=0;j<brows;j++){
			for(k=0;k<brows;k++){
				sum+=a[i][k]*b[k][j];
			}
			product[i][j]=sum;
			sum=0;
		}
	}

	//printing the result
    printf("Multiplication of matrices:\n");
    for(i=0;i<arows;i++){
    	for(j=0;j<bcols;j++){
    		printf("%d",product[i][j]);
    		printf(" ");
		}
		printf("\n");
	}

	//Addition of matrices
	 for(i=0;i<arows;i++){
	 	for(j=0;j<bcols;j++){
	 	addition[i][j]=a[i][j]+b[i][j];
		 }
}

	 //printing the addition
	 printf("Addition of matrices:\n");
	 for(i=0;i<arows;i++){
	 	for(j=0;j<bcols;j++){
	 		printf("%d",addition[i][j]);
	 		printf(" ");
		 }
		 printf("\n");
	 }
}
